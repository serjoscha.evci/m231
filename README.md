# M231 - Datenschutz und Datensicherheit anwenden

Setzt Datenschutz und Datensicherheit bei Informatiksystemen ein. Überprüft vorhandene Systeme auf Einhaltung von Richtlinien.

# Leistungsbeurteilung

Alle Informationen zu der Leistungsbeurteilung finden Sie [hier](/00_evaluation/README.md). 
 
 # Unterlagen
  - Gitrepository [ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)
  - Gitbook [ch-tbz-it.gitlab.io/Stud/m231/](https://ch-tbz-it.gitlab.io/Stud/m231/)
 - Unterlagen die aus Urheberrechtlichen Gründen nicht hier veröffentlicht werden können, sind im MS Teams zu finden. 