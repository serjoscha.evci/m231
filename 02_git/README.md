# Persönliches Portfolio mit GIT und Markdown <!-- omit in toc -->

Für die LB-2 erstellen Sie ein persönliches Portfolio in Form eines GIT-Repositories. In diesem Block bereiten Sie Ihr persönliches Notebook vor und erlernen die Grundlagen von *Markdown* und *GIT*. 

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Ziele](#1-ziele)
- [2. Versionsverwaltung mit GIT](#2-versionsverwaltung-mit-git)
  - [2.1. Aufgabenstellung](#21-aufgabenstellung)
  - [2.2. Versionsverwaltung in der Informatik](#22-versionsverwaltung-in-der-informatik)
  - [2.3. Was ist GIT?](#23-was-ist-git)
  - [2.4. Verständnisfragen](#24-verständnisfragen)
- [3. Eigenen Laptop für die Verwendung von GIT vorbereiten](#3-eigenen-laptop-für-die-verwendung-von-git-vorbereiten)
  - [3.1. Anleitung](#31-anleitung)
- [4. Account auf gitlab mit TBZ E-Mail Adresse erstellen](#4-account-auf-gitlab-mit-tbz-e-mail-adresse-erstellen)
  - [4.1. Anleitung](#41-anleitung)
- [5. Neues Repository für persönliches Portfolio erstellen](#5-neues-repository-für-persönliches-portfolio-erstellen)
- [6. Markdown Syntax kennenlernen](#6-markdown-syntax-kennenlernen)
  - [6.1. Aufgabenstellung](#61-aufgabenstellung)
  - [6.2. Links](#62-links)
- [7. Git Branching](#7-git-branching)
  - [7.1. Aufgabenstellung](#71-aufgabenstellung)
- [Hilfreiche Links](#hilfreiche-links)

# 1. Ziele
 - Sie haben ein GIT-Repository für Ihr persönliches Portfolio erstellt und mit einem Remote-Repository synchronisiert. 
 - Sie kennen die wichtigsten am häufigsten verwendeten Markdown Elemente
 - Sie kennen die wichtigsten GIT-Befehle und ihren Syntax (add, commit, push, pull, status, log).
 - Sie haben einen Quelletext-Editor ausgewählt, auf ihrem persönlichen Notebook eingerichtet und kennen die wichtigsten Bedienelemente. 

# 2. Versionsverwaltung mit GIT
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Einarbeitung in neues Thema |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen was *GIT* ist, wie es grundsätzlich funktioniert und weshalb es für Sie als Plattformentwickler ein wichtiges Tool ist.  |

## 2.1. Aufgabenstellung
Erreichen Sie das Ziel des Arbeitsauftrages indem Sie sich in das Thema Versionverwaltung und GIT einlesen. Beantworten Sie anschliessend die Verständnisfragen. 

## 2.2. Versionsverwaltung in der Informatik
Versionsverwaltung ist heute in der Softwareentwicklung nicht mehr wegzudenken. Die manuelle Nachverfolgung von Änderungen und die mühsame Zusammenführung von Quellcode wurde früh als eine Aufgabe angesehen, die vom Computer erledigt werden soll. Einer der frühstens Versionsverwaltungssoftware für UNIX wurde 1972 in Bell Labs entwickelt. (In diesem Labor in der Nähe von New York wurden viele Durchbrüche in der Telekommunikationstechnik, Mathematik, Physik und Materialforschung erzielt.) 1982 folgte das *Revision Control System* (kurz RCS)[^1], gefolgt vom bereits heute noch bekannten *Concurrent Version Control* (kurz CVS). Bevor Linus Torvalds 2005 Git veröffentlichte dominierten andere Applikationen den Markt, wie Subversion, Perforce oder BitKeeper.

[^1]: <https://www.cs.purdue.edu/homes/trinkle/RCShome/>

Einige von diesen Systemen, insbesondere BitKeeper, würden sich für einen jungen Git User wie eine Zeitreise anfühlen. Versionskontrollsoftware vor Git funktionierte nach einem grundsätzlich anderen Paradigma. In einer Taxonomie[^2] von Eric Sink, Author von *Version Control by Example*, ist Git eine Versionskontrollsoftware der dritten Generation, während die meisten Vorgänger, die in den 1990er und 2000er populär waren, zur zweiten Generation angehören. Der wesentliche Unterschied besteht darin, dass die *2nd Generation* zentralisiert (centralized) und die *3rd generation version control systems* verteilt funktioniert.

[^2]: Einheitliches Verfahren oder Modell (Klassifikationsschema) mit dem Objekte nach bestimmen Kriterien klassifiziert werden.

Versionskontrolle ist nicht nur in der Softwareentwicklung praktisch. Applikationen aller Arten, wie Word, Excel, Photoshop, verfolgen die Änderungen des Benutzers und geben ihm die Möglichkeit diese Rückgängig zu machen. Filehosting-Dienste wie Dropbox, die es ermöglichen einen Ordner auf mehreren Geräten zu spiegeln müssen ebenfalls Versionskontrolle betreiben, indem Sie feststellen, wenn eine identische Datei auf zwei verschiedenen Geräten gleichzeitig verändert wurde.

In der Systemtechnik kommen mit Software-defined Infrastructure (kurz SDI oder) und *Infrastructure as Code* immer mehr komplexe und mächtige Konfigurationsdateien zum Einsatz. In einer einzigen Datei lassen sich ganze Netzwerktopologien oder Serverinfrastrukturen festlegen. Auch hier eignet sich Versionskontrollsoftware besonders gut, um Änderungen ohne grossen Aufwand nachverfolgen zu können und die Teamarbeit zu vereinfachen. Beispiele dafür findet man bei cloud-init[^3], vagrant oder docker[^4].

[^3]: <https://cloudinit.readthedocs.io/en/latest/>

[^4]: <https://docs.docker.com/compose/>

Das Ziel dieser Übung ist, dass Sie sich mit dem Thema Versionskontrolle auseinandersetzen und die Vorzüge kennenlernen. Wenn Sie mehr über die Geschichte der Versionskontrolle erfahren möchten, empfehle ich Ihnen diese beiden Links[^5]. Weitere interessante Informationen finden Sie hier[^6].

[^5]: <https://dev.to/thefern/history-of-version-control-systems-vcs-43ch> <https://twobithistory.org/2018/07/07/cvs.html>

[^6]: <https://bitbucket.org/product/de/version-control-software>

## 2.3. Was ist GIT?
GIT ist ein sogenanntes Versionskontrollsystem (VCS) und wurde Anfang 2005 von Linus Torvalds, dem Initiator des Linux-Kernels, entwickelt. Es erstaunt deshalb nicht, dass GIT konzeptionell ähnlich aufgebaut ist wie ein Linux-Filesystem.

Einen guten Einstieg in das Thema GIT erhalten Sie unter den folgenden Links:
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - https://dev.to/milu_franz/git-explained-the-basics-igc
 - https://hackernoon.com/understanding-git-fcffd87c15a3


## 2.4. Verständnisfragen
 - Was bedeutet die Abkürzung DVCS? 
 - Wie heisst das bekannteste DVCS System? 
 - Was ist der Unterschied zwischen einer zentralisierten und einer dezentralisierten Versionsverwaltung? 
 - Was bedeutet in diesem Zusammenhang die Abkürzung "MD"?
 - Weshalb ist "MD" in Zusammenspiel mit DVCS so nützlich?
 - Was ist wesentlich umfangreicher wie "MD" und wird sehr gerne für wissenschaftliche Arbeiten verwendet (z.B. Ihre zukünftige Bachelorarbeit an einer Hochschule)? 
 - Weshalb ist der Einsatz von docx Dateien mit dem bekanntesten DVCS System problematisch?


# 3. Eigenen Laptop für die Verwendung von GIT vorbereiten
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie man GIT auf einem Notebook installiert.<br>Sie wissen wie man einen universalen Quelltext-Editor auf einem Notebook installiert. |

## 3.1. Anleitung
 - Laden Sie sich von https://git-scm.com/ *Git für Windows* in der aktuellsten Version herunter und installieren Sie es auf Ihrem persönlichen Notebook.
 - Laden Sie sich [Visual Studio Code](https://code.visualstudio.com/) herunter und installieren Sie es auf Ihrem persönlichen Notebook.
 - Installieren Sie in Visual Studio Code die folgenden Plugins: [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced), [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one), [Excel to Markdown table](https://marketplace.visualstudio.com/items?itemName=csholmq.excel-to-markdown-table)


# 4. Account auf gitlab mit TBZ E-Mail Adresse erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie Sie einen persönlichen GitLab-Account erstellen<br>Sie wissen wie man einen SSH-Key generiert.<br>Sie wissen wie Sie einen SSH Key auf GitLab hinterlegen können. |

**Wichtig: Für diesen Arbeitsauftrag müssen Sie auf Ihrem Notebook Git for Windows installiert haben!**

## 4.1. Anleitung
Eine ausführliche Anleitung zu SSH Keys erhalten Sie auf auf der GitLab Seite [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/). Die wichtigsten Schritte dabei sind: 

 - Erstellen Sie auf https://about.gitlab.com/ einen persönlichen GitLab-Account mit Ihrer TBZ E-Mail-Adresse. 
 - Öffnen Sie *Git Bash* und führen Sie folgenden Befehl aus, um Ihren persönlichen :
```bash
ssh-keygen.exe -t ed25519  -C "IHRE E-MAIL ADRESSE"
```
 - Geben Sie den öffentlichen Teil ihres Keys auf Ihrem Terminal aus. Zeigt es etwas an?
```bash
cat .ssh/id_ed25519.pub
```
 - Hinterlegen Sie den Key in Ihrem Gitlab Account: Siehe https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account


# 5. Neues Repository für persönliches Portfolio erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen wie man ein Projekt auf gitlab erstellt und mit einem GIT Repository auf Ihrem lokalen PC verknüpft. |

Es gibt verschiedene Wege wie Sie ein neues Repository erstellen können. Grundsätzlich handelt es sich um einen Ordner auf Ihrem Computer den Sie mithilfe von GIT mit einem Server bzw. *Remote-Repository* synchronisieren können. Im Vergleich zu einem synchronisierten Ordner mit OneDrive, Google Drive, Dropbox oder ähnlich erfolgt die Synchronisierung nicht automatisch. 

Auf [dieser Webseite](https://github.com/ser-cal/M300-Git-Repo-Setup#git-lokal-einrichten-und-ein-erstes-repository-initialisieren-und-synchronisieren) finden Sie eine Anleitung wie Sie ein neues Repository initialisieren können. 

# 6. Markdown Syntax kennenlernen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie sind in der Lager ein Dokument in Markdown zu gestalten.<br>Sie kennen die wichtigsten Markdown Elemente.<br> |

## 6.1. Aufgabenstellung
Gestalten Sie ein Markdown Dokument, dass gerendert genauso aussieht wie «Markdown Example.html». Informieren Sie sich dafür selbstständig über den Markdown Syntax. 

## 6.2. Links
 - https://www.markdownguide.org/basic-syntax/
 - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

# 7. Git Branching
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie kennen die wichtigsten GIT-Befehle.<br>Sie haben ein grundsätzliches Verständnis von *commits*<br> |

## 7.1. Aufgabenstellung
Mithilfe des interaktive Tutorial [Learn Git Branching](https://learngitbranching.js.org) lernen Sie spielerisch den Umgang mit GIT. Spielen Sie die einzelnen Levels durch. 

Empfohlene Levels:
 - Local: Introduction Sequence
 - Local: Ramping Up
 - Local: Moving Work Around
 - Local: A Mixed Bag
 - Remote: Push & Pull


# Hilfreiche Links
 - https://devhints.io/bash
 - https://zach-gollwitzer.medium.com/the-ultimate-bash-crash-course-cb598141ad03
 - 