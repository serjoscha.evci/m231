# Leistungsbeurteilung Modul 231 (V1)

Die Schlussnote setzt sich wie folgt zusammen:
```
N = LB1*0.3 + LB2*0.7
```

# LB1 - Schriftliche Prüfung

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | Selbstgeschriebene Zusammenfassung |
| Nicht erfüllen der Zulassungsbedingungen | 1 Note Abzug |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochene Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

## Persönliche Zusammenfassung
 - Umfang:
   - Ausgedruckt: Max. 2 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 4 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.
   - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Mindestumfang: Wichtigste Punkte jedes Prüfungsrelevanten Dokumentes
 - Zusammenfassung muss mit der Prüfung abgegeben werden!

# LB2 - Persönliche Dossier (V1, 22.08.2021)

 - Abgabedatum für persönliches Lernportfolio ist aus dem Teams Channel zu entnehmen.
 - Liste für Besprechungstermine finden Sie im Teams

| N° | Kriterien  | Max  |
|----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
| 1  | Alle Besprechungstermine bis am 06.09.2021 vereinbart. | 2 P  |
| 2  | Zum ersten Besprechungstermin pünktlich erschienen (1P) und diesen erfolgreich durchgeführt. (3P)  | 4 P  |
| 3  |Zum zweiten Besprechungstermin pünktlich erschienen (1P) und diesen erfolgreich durchgeführt (Repository ist fertig und kann präsentiert werden (1P), Applikationen auf dem Laptop geöffnet und bereit zum präsentieren (1P), Alle Aussagen des Lernede(r) sind fachlich korrekt und sachbezogen (1P) ). | 4 P  |
| 4  | Persönliche Tages- und Wochenziele und jeweilige Auswertung (Was habe ich erreicht? Wo hatte ich Schwierigkeiten? Wo muss ich mich noch verbessern?) fortlaufend im persönlichen Portfolio festgehalten. (1 P pro Tag (maximal 6), Voraussetzung: Kontinuität und Regelmässigkeit müssen in den Commits ersichtlich sein, sonst keine Punkte).                                                                                            | 8 P  |
| 5  | Github oder Gitlab Account angelegt (1P), Repository für eigenes Portfolio angelegt (1P), Name des Repository bezieht sich auf den Inhalt und ist nachvollziehbar (1P), Repository der Lehrperson freigeben (1P)                                                                                                                                                                                                             | 4 P  |
| 6  | Das persönliche Repository enthält ein README.md (1P), sinnvolle Struktur gewählt (Unterseiten) (1P), Navigation vorhanden (1P), alle Unterseiten verlinkt (1P), Navigation ist intuitiv (1P)                                                                                                                                                                                                                                    | 5 P  |
| 7  | Individuelles Programm (Desktop, Mobiltelefon-App oder Webapplikation/Service) hinsichtlich Datenschutz analysiert und wichtigste Punkte (Wo werden die Daten gespeichert, Zu welchem Zweck werden meine Daten verwendet, Werden die Daten an Dritte weitergegeben und zu welchem Zweck, Welche Metriken werden wie häufig und zu welchem Zweck erfasst, usw.) <br/>Analysiertes Programm muss mit Lehrperson abgesprochen sein! | 4 P  |
| 8  | Passwortverwaltungsprogramm ausgewählt (1P), Entscheid begründet (Vergleich mit anderer Software, Vor und Nachteile) (1P)                                                                                                                                                                                                                                                                                                        | 2 P  |
| 9  | Passwortverwaltungsprogramm auf eigenen Gerät installiert und der Lehrperson gezeigt.                                                                                                                                                                                                                                                                                                                                            | 4 P  |
| 10 | Ablagekonzept entwickelt: Unterschiedliche Daten (Eigene Daten auf dem Persönlichen- oder Geschäftslaptop) nach Kriterien (Schutzwürdigkeit) eingeteilt (1P), Unterschiedliche Speicherorte analysiert und Datenschutz beurteilt (1P), (Ablage- /Ordner-)Struktur entwickelt und dokumentiert.                                                                                                                                   | 2 P  |
| 11 | Backupstrategie definiert (2P) und umgesetzt (2P)                                                                                                                                                                                                                                                                                                                                                                                | 4 P  |
| 12 | Individuelle Quelle (Datenschutzgesetzt, Leitfaden, o.ä.) und wichtigste Punkte aussagekräftig zusammengefasst. Quelle muss mit Lehrperson abgesprochen sein.                                                                                                                                                                                                                                                                    | 4 P  |
| 13 | Quellen zu Entscheidungsgrundlagen und Links (z.B. zu Programmen) sind immer vorhanden (3 P für Vollständig, 2 P für Mehrheitlich, 1P für teilweise, 0 P für  | 3 P  |
| 14 | Zwei Checklisten (pro Checkliste 1P) des Datenschutzbeauftragten des Kanton Zürichs bearbeitet und gemäss Aufgabenbeschreibung dokumentiert. | 2 P |
| 15 | Zusatzpunkte kann es zum Beispiel geben für:<br/>Kritische Hinterfragung mit fachlicher Differenzierung von Datenschutzgesetzen/Datenschutzbestimmungen; Umfangreiches Ablagekonzept, welches alle Daten umfasst, die vom Lernenden bearbeitet werden; Mehrstufiges Backupkonzept, dass gegen alle Risiken bestmöglich standhält; Sehr gute Übersichtlichkeit und Darstellung; Kein Ballast; usw.                                | 10 P  |
|    | **Total** | 60 P |