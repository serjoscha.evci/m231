# Schutzwürdigkeit von Daten im Internet

# Wichtigste Begriffe klären
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit (3 bis 4 Personen) |
| Aufgabenstellung  | Erfahrungsaustausch |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Wichtigste Fragen gemeinsam Diskutieren, Wichtigste Punkte auf Poster festhalten |

## Aufgabenstellung
Setzen Sie sich in der Gruppe zusammen, diskutieren und recherchieren Sie gemeinsam zum Thema Datenschutzgesetz. Notieren Sie die wichtigsten Erkenntnisse auf einem Poster (Sie dürfen ihre Erkenntnisse auch grafisch festhalten). Anschliessend präsentiert jemand der Gruppe das Poster. Nutzen Sie als Grundlage / Inspiration die nachfolgenden Fragen: 

Jede Gruppe diskutiert folgende Fragen und hält die wichtigsten Punkte stichwortartig auf dem Poster fest. 
 - Ist Datenschutz wichtig?
 - Weshalb ist Datenschutz für mich wichtig?
 - Vor was schützt Sie das Datenschutzgesetz?
 
Jede Gruppe erhält von der Lehrperson eine Nummer zugeteilt. Beantworten Sie nur die Frage mit Ihrer Nummer.

 1. Was ist ein Meme? Welche Memes sind erlaubt, welche nicht? Gibt es Regeln?
 2. Wo finde ich das Datenschutzgesetz? Von wann ist die aktuelle Fassung? Was steht da so drin?
 3. Welche Länder haben einen gleichwertiges Datenschutzgesetz? Was sind meine Rechte (Stichwort Auskunftsrecht)?
 4. Was ist Cybermobbing? Hat Cybermobbing etwas mit Datenschutz zu tun? Begründen Sie. 
 5. Was ist sicherer: Telegram, Signal, Whats-App? Weshalb?
 6. Was ist Identitätsdiebstahl? Was sind die Folgen?


# Nutzungsbestimmungen bekannter Plattformen prüfen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit, Gruppenarbeit |
| Aufgabenstellung  | Recherche |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Nutzungsbestimmungen von Sozialen Netzwerken finden. Jeder lernende weiss, welche die wichtigsten und am häufigsten beschrieben Punkte in solchen Nutzungsbestimmungen sind.  |

## Aufgabenstellung
 - Pro Gruppe sollen 3 bis 4 Personen eine Plattform recherchieren. Beispiel für Plattformen: Reddit, Twitter, Instagram, TikTik, Discord, Signal, Whats-App, ...)
 - Die Plattformen werden unter den Lernenden aufgeteilt.
 - Zu jeder Plattform sollen folgende Fragen beantwortet werden:
   - Wo finde ich die Datenschutz-/Nutzungsbestimmungen (URL)?
   - Welche Daten (z.B. Personendaten) werden gespeichert?
   - Für was werden die Daten verwendet?
   - Werden die Daten weitergegeben? Wenn ja, an wen?
   - Darf der Dienst/Service/Plattform meine Bilder/Videos weiterverkaufen?
 - Die Ergebnisse werden anschliessend auf einem gemeinsamen Powerpoint festgehalten. Pro Plattform 1 bis max. 2 Folien. 


# Einführung in das Thema Datenschutz
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Plenum |
| Aufgabenstellung  | Präsentation und Erklärungen der Lehrperson verfolgen und bei bedarf Notizen machen. |
| Zeitbudget  |  1 Lektion |
| Ziel | Jeder Lernende kennt nicht rechtliche Grundlagen zum Datenschutz und weiss was Sorgfaltspflicht für Ihn als Informatiker bedeutet   |

# Checklisten des Datenschutzbeauftragten des Kantons Zürichs
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Prüfen der eigenen Infrastruktur mithilfe von Checklisten |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende hat einen Teil seiner persönlichen Infrastruktur geprüft, protokolliert und die notwendigen Verbesserungsmassnahmen festgelegt und wenn möglich umgesetzt.  |

## Aufgabenstellung
 - Besuchen Sie die Webseite https://www.datenschutz.ch/meine-daten-schuetzen
 - Wählen zwei Checklisten aus und gehen Sie diese Schrittweise durch. 
 - Notieren Sie zu jedem Punkt, ob Sie
   - das bereits erfüllt haben.
   - Wenn ja: Wie haben diesen Punkt erfüllt?
   - Wenn nein: Was können Sie machen um diesen Punk zu erfüllen?
 - Wichtig: Sie müssen auch wissen, wie sie es umsetzen! (Siehe Beispiel unten)
 - Halten Sie Ihre Ergebnisse im Markdown-Format in einem Textdokument fest. Das wird später Teil Ihres Portfolios werden. 

## Markdownformat-Beispiel 
Ihre Dokumentation sollte ungefähr so aussehen:
```
# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Checkliste Smartphone-Sicherheit erhöhen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen
 - 01 Gerätesperren aktivieren
    - Ja, ich habe eine Gerätesperre mit PIN aktiviert. 
 - 02 Gerät bei Verlust sofort sperren und löschen lassen.
    - Kannte ich noch nicht, habe https://www.google.com/android/find besucht und mir die funktionen angeschaut. Jetzt weiss ich wie ich im Ernstfall handeln muss. 
 - 03 Apps aus vertrauenswürdigen Quellen installieren
    - Ich installiere alle Apps aus dem Google Playstore und prüfe bei jeder App, welche Zugriffsrechte Sie hat. Ich bin alle Berechtigungseinstellungen für jedes App im meinem Telefon durchgegangen und wenn nötig Änderungen vorgenommen
 - 04 ...
```

# Auskunft über eigene Personendaten verlangen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Öffentliche Webseiten zum Thema Auskunftsrecht besuchen und Musterbrief studieren |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende weiss wo er Informationen zu seinem Auskunftsrecht findet und kennt das Verfahren  |

## Aufgabenstellung
 - Besuchen Sie die beiden Webseiten https://www.datenschutz.ch/meine-rechte-einfordern und https://www.edoeb.admin.ch/edoeb/de/home/dokumentation/datenschutz/musterbriefe/wie-sie-vorgehen--wenn-sie-ein-auskunftsbegehren-stellen-moechte.html
 - Angenommen Sie wollen wissen, welche Personendaten Ihr Mobilfunkanbieter über Sie gespeichert hat. **Wie müssen Sie vorgehen?**
 - Laden Sie sich den Musterbrief herunter und füllen Sie diesen Proforma aus. 


# Quellen
 - https://www.srf.ch/sendungen/myschool/datenschutz-2
 - https://www.digitale-gesellschaft.ch/auskunftsbegehren/
 - https://www.fedlex.admin.ch/eli/cc/1993/1945_1945_1945/de
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.bj.admin.ch/dam/bj/de/data/staat/gesetzgebung/datenschutzstaerkung/vorentw-d.pdf.download.pdf/vorentw-d.pdf
 - https://www.bj.admin.ch/bj/de/home/staat/gesetzgebung/datenschutzstaerkung.html
 - https://www.kmu.admin.ch/kmu/de/home/aktuell/news/2021/neues-datenschutzgesetz-kmu-muessen-sich-anpassen.html
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.economiesuisse.ch/de/artikel/datenschutzgesetz-bereiten-sie-sich-jetzt-vor
 - https://www.economiesuisse.ch/de/datenwirtschaft
 - https://www.rosenthal.ch/downloads/Rosenthal-Webinar-DSG-GDPR.pdf
 - https://www.myright.ch/de/business/rechtstipps/datenschutz/dsg-neu
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz.html
 - https://www.stadt-zuerich.ch/pd/de/index/stadtpolizei_zuerich/praevention/digitale-medien/Gefahren/falsche-identitaeten.html
 - https://www.verbraucherzentrale.de/wissen/digitale-welt/datenschutz/welche-folgen-identitaetsdiebstahl-im-internet-haben-kann-17750
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - [https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt)](https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt))
 - https://www.20min.ch/story/das-bedeutet-das-neue-eu-gesetz-fuer-deine-memes-755504826807
 - https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A02019L0790-20190517&qid=1629634471866
 - https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/verbraucherinnen-und-verbraucher_node.html
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/ueberblick/datenschutz.html